# PCS Project - 2D

## 2D Problem

Create a C++ function which given an input concave polygon it returns the list of convex polygons.  

The C++ function to be implemented is the following
```c++
/// Function to convert a concave polygon to a list of convex polygons
/// @param[in] concavePolygon - the concave polygon
/// @param[out] convexPolygons - the list of convex polygons
static Output::ExitCodes ConcaveToConvexPolygon(const IPolygon& concavePolygon, list<IPolygon*>& convexPolygons);
```

## Tests

### Test One

The following image shows the first 2D test:

![2DTestOne](Images/PolygonTestOne.jpg)

### Test Two

The following image shows the second 2D test:

![2DTestTwo](Images/PolygonTestTwo.jpg)

### Test Three

The following image shows the third 2D test:

![2DTestThree](Images/PolygonTestThree.jpg)


## Solution Example

The results is not unique, it depends from the algorithm implemented.

A possible solution of the Test Two is reported in the following image:

![2DTestTwo](Images/PolygonTestTwo_Result.jpg)

## Code

See [Wiki](https://bitbucket.org/FabioVicini/pcs-2d/wiki/Home) for code information.

## Hints

The input / output of the C++ program can be mixed with MATLAB software.
The following hints shows are to be used in MATLAB.

### Generate Other Tests

The following code generates a 2D random polygon.

```matlab
npts = 5;
pts = randn(npts, 2);
```

### Show Results

Given a list of points `pts`, it is possible to show them on a plot using the following code:

```matlab
fill(pts(:,1), pts(:,2), 'r', 'facealpha',0.5);
```